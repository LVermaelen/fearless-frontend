# Necessary Software to run this application:
- Docker

-----------------------------------------------

# To Start Up:

- Fork and clone the repository into your local system

- In the project directory in your terminal run the following commands:
    * docker compose build
    * docker compose up

- Once the docker containers are running, navigate to localhost: 3000 to see the webpage.

- To create a conference, follow the following steps:
    * navigate to the New Location tab in the nav bar

    * create a new location

    * navigate to the New Conference tab to create a new conference

- You can also make a request to present at a specific conference at the New Presentation tab and Attend a conference by navigating to the Attend tab.

--------------------------------------------------------

# Project Information:

- Backend

    * Language: Python
    * Framework: Django

- Frontend

    * Language: Javascript
    * Framework: React

This application was built to help manage conferences all over the country. It allows the user to create a conference, attend the conference, and even apply to presentation at a particular conference. I utilized a microservice architecture by way of rabbitMQ and Cronjobs to account for easier scaling in the future. I am currently working on deploying this app for real world use.
