import React from 'react';

class ConferenceForm extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            name: "",
            starts: "",
            ends: "",
            description: "",
            max_presentations: "",
            max_attendees: "",
            location: "",
            locations: []
        };
    }

    handleSubmit = async (event) => {
        event.preventDefault();
        const data = {...this.state}
        // data.max_presentations = data.maxPresenations
        // data.max_attendees = data.maxAttendees
        // delete data.maxPresenations
        // delete data.maxAttendees
        delete data.locations

        const conferenceUrl = 'http://localhost:8000/api/conferences/';
        const response = await fetch(conferenceUrl, {
                method: 'post',
                body: JSON.stringify(data),
                headers: {
                'Content-Type': 'application/json',
                },
        });

        if (response.ok) {
            const newConference = await response.json()
            console.log(newConference)
            const cleared = {
                name: "",
                starts: "",
                ends: "",
                description: "",
                max_presentations: "",
                max_attendees: "",
                location: "",
            };
            this.setState(cleared);

        }
    }

    handleInputChange = (event) => {
        const name = event.target.name
        const value = event.target.value
        this.setState({...this.state, [name]: value})
    }

    // handleNameChange = (event) => {
    //     const value = event.target.value;
    //     this.setState({name: value})
    // }

    // handleStartChange = (event) => {
    //     const value = event.target.value
    //     this.setState({starts: value})
    // }

    // handleEndChange = (event) => {
    //     const value = event.target.value
    //     this.setState({ends: value})
    // }

    // handleDescriptionChange = (event) => {
    //     const value = event.target.value
    //     this.setState({description: value})
    // }

    // handleMaxPresentationChange = (event) => {
    //     const value = event.target.value
    //     this.setState({maxPresenations: value})
    // }

    // handleMaxAttendeesChange = (event) => {
    //     const value = event.target.value
    //     this.setState({maxAttendees: value})
    // }

    // handleLocationChange = (event) => {
    //     const value = event.target.value
    //     this.setState({location: value})
    // }

    async componentDidMount() {
        const url = 'http://localhost:8000/api/locations/';
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            this.setState({locations: data.locations})
    }
}

    render() {
        return (
            <div className="container">
                <div className="row">
                    <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Create a new conference</h1>
                        <form onSubmit={this.handleSubmit} id="create-conference-form">
                        <div className="form-floating mb-3">
                            <input onChange={this.handleInputChange} value={this.state.name} placeholder="Name" required type="text" name="name" id="name" className="form-control"/>
                            <label htmlFor="name">Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={this.handleInputChange} value={this.state.starts} placeholder="Starts" required type="date" name="starts" id="starts" className="form-control"/>
                            <label htmlFor="starts">Start Date</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={this.handleInputChange} value={this.state.ends} placeholder="Ends" required type="date" name="ends" id="ends"  className="form-control"/>
                            <label htmlFor="ends">End Date</label>
                        </div>
                        <div className="mb-3">
                            <textarea onChange={this.handleInputChange} value={this.state.description} placeholder="Description" name="description" rows="3" id="description" className="form-control"></textarea>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={this.handleInputChange} value={this.state.max_presentations} placeholder="Max_presentations" required type="number" name="max_presentations" id="max_presentations" className="form-control"/>
                            <label htmlFor="max_presentations">Max Presentations</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={this.handleInputChange} value={this.state.max_attendees} placeholder="Max_attendees" required type="number" name="max_attendees" id="max_attendees" className="form-control"/>
                            <label htmlFor="max_attendees">Max Attendees</label>
                        </div>
                        <div className="mb-3">
                            <select onChange={this.handleInputChange} value={this.state.location} name="location" required id="location" className="form-select">
                            <option value="">Choose a Location</option>
                            {this.state.locations.map(location => {
                                return (
                                    <option key={location.id} value={location.id}>{location.name}</option>
                                );
                            })}
                            </select>
                        </div>
                        <button className="btn btn-primary">Create</button>
                        </form>
                    </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default ConferenceForm;
