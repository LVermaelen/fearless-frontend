import Nav from "./Nav";
import React from "react";
import {
  BrowserRouter,
  Routes,
  Route,
} from 'react-router-dom';
import LocationForm from "./LocationForm";
import AttendeesList from "./AttendeesList";
import ConferenceForm from "./ConferenceForm";
import AttendConferenceForm from "./AttendConferenceForm";
import PresentationForm from "./PresentationForm";
import MainPage from "./MainPage";


function App(props) {
  return (
    <BrowserRouter>
    <Nav />
      <Routes>
        <Route index element={<MainPage />}/>
        <Route path="presentations">
          <Route path="new" element={<PresentationForm />}/>
        </Route>
        <Route path="attendees/new" element={<AttendConferenceForm />}/>
        <Route path="conferences">
          <Route path="new" element={<ConferenceForm />}/>
        </Route>
        <Route path="locations">
          <Route path="new" element={<LocationForm />} />
        </Route>
        <Route path="attendees" element={<AttendeesList attendees={props.attendees}/>}/>
      </Routes>
  </BrowserRouter>
  )
}

export default App;
