import React from 'react';

class PresentationForm extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            presenter_name: "",
            presenter_email: "",
            company_name: "",
            title: "",
            synopsis: "",
            conference: "",
            conferences: []
        }
    }

    handleSubmit = async (event) => {
        event.preventDefault();
        const data = {...this.state}
        delete data.conferences

        const presentationUrl = `http://localhost:8000/api/conferences/${data.conference}/presentations/`;
        const response = await fetch(presentationUrl, {
                method: 'post',
                body: JSON.stringify(data),
                headers: {
                'Content-Type': 'application/json',
                },
        });

        if (response.ok) {
            const newPresentation = await response.json()
            console.log(newPresentation)
            const cleared = {
                presenter_name: "",
                presenter_email: "",
                company_name: "",
                title: "",
                synopsis: "",
                conference: "",
            };
            this.setState(cleared);

        }
    }

    handleInputChange = (event) => {
        const name = event.target.name
        const value = event.target.value
        this.setState({...this.state, [name]: value})

    }

    async componentDidMount() {
        const url = 'http://localhost:8000/api/conferences/';
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            this.setState({conferences: data.conferences})
        }
    }

    render() {
        return (
            <div className="container">
                <div className="row">
                    <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Create a new presentation</h1>
                        <form onSubmit={this.handleSubmit} id="create-presentation-form">
                        <div className="form-floating mb-3">
                            <input onChange={this.handleInputChange} value={this.state.presenter_name} placeholder="Presenter_name" required type="text" name="presenter_name" id="presenter_name" className="form-control"/>
                            <label htmlFor="presenter_name">Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={this.handleInputChange} value={this.state.presenter_email} placeholder="Email" required type="email" name="presenter_email" id="presenter_email" className="form-control"/>
                            <label htmlFor="email">Email</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={this.handleInputChange} value={this.state.company_name} placeholder="Company_name" name="company_name" required type="name" id="company_name" rows="3" className="form-control"/>
                            <label htmlFor="company_name">Company Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={this.handleInputChange} value={this.state.title} placeholder="Title" name="title" required type="name" id="title" rows="3" className="form-control"/>
                            <label htmlFor="title">Title</label>
                        </div>
                        <div className="mb-3">
                            <textarea onChange={this.handleInputChange} value={this.state.synopsis} placeholder="Synopsis" required type="text" name="synopsis" id="synopsis" className="form-control"></textarea>
                        </div>
                        <div className="mb-3">
                            <select onChange={this.handleInputChange} value={this.state.conference} name="conference" required id="conference" className="form-select">
                            <option value="">Choose a Conference</option>
                            {this.state.conferences.map(conference => {
                                return (
                                    <option key={conference.id} value={conference.id}>{conference.name}</option>
                                );
                            })}
                            </select>
                        </div>
                        <button className="btn btn-primary">Create</button>
                        </form>
                    </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default PresentationForm;
